import uuid
import argparse
import os
from distutils.dir_util import copy_tree
from merico.analysis.analysis_tasks import analyze_others, analyze_ccg, analyze_commits, analyze_first_order, \
    analyze_aspect, analyze_code_quality
from merico.analysis.project import Project, GitAuthType
from merico.analysis.report.protobuf_writer import ProtobufWriter
from merico.db.ca_models import CA_PROJECT_SOURCE_TYPE_DEFAULT
from merico.util.task_constants import SUBMIT_TYPE_ANALYZE, SUBMIT_TYPE_ASPECT_ANALYZE, SUBMIT_TYPE_CODE_QUALITY_ANALYZE
from merico.analysis.report.protobuf_reader import ProtobufReader
from merico.util.crypt import encrypt_str


def gen_analyze_report(git_url, auth_type = GitAuthType.UNKNOWN, private_key = None):
    print(git_url)
    source_type = CA_PROJECT_SOURCE_TYPE_DEFAULT
    analysis_uuid = str(uuid.uuid4())
    source_id = analysis_uuid
    print(analysis_uuid)
    project: Project = Project(git_url=git_url,
                               analysis_type=SUBMIT_TYPE_ANALYZE,
                               analysis_uuid=analysis_uuid,
                               source_type=source_type,
                               source_id=source_id,
                               auth_type=auth_type,
                               private_key=private_key)
    project.update()
    analyze_first_order(project=project)
    analyze_commits(project=project)
    analyze_ccg(project=project)
    analyze_others(project=project)
    project.dump_meta()
    pb_writter = ProtobufWriter(project)
    pb_writter.dump()
    return project.storage.protobuf_report_folder



if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Generate report', formatter_class=argparse.MetavarTypeHelpFormatter)
    parser.add_argument('--privatekey', type=str, default=None, help='Private key for ssh clone',nargs=1)
    parser.add_argument('--repos', type=str, default=None, help='Comma separated repositories to process',nargs=1)
    parser.add_argument('--no-db', default=False, action="store_true", help='Skip load to database')
    parser.add_argument('--protobuf-path', type=str, default=None, help='Copy protobuf output to folder')
    args = parser.parse_args()
    if args.privatekey is not None:
        ssh_repos = [
            'git@gitlab.com:huiqingu/meta-analytics.git',
            'git@gitlab.com:meri.co/vdev.co.git'
        ]
        for repo in ssh_repos:
            with open(args.privatekey[0], 'r') as f:
                private_key_str = f.read()
                private_key = encrypt_str(private_key_str)
                protobuf_folder = gen_analyze_report(git_url=repo, auth_type=GitAuthType.SSH, private_key=private_key)
                print(protobuf_folder)
                private_key = encrypt_str(private_key_str)
                protobuf_folder = gen_analyze_report(git_url=repo, auth_type=GitAuthType.SSH, private_key=private_key)
                print(protobuf_folder)
                pr = ProtobufReader(protobuf_folder)
                pr.load_to_database()

    repos = [
        'https://github.com/TheAlgorithms/Python.git',
        'https://github.com/TheAlgorithms/Java.git',
        'https://github.com/TheAlgorithms/C.git',
        'https://github.com/TheAlgorithms/Go.git',
        'https://github.com/TheAlgorithms/Javascript.git',
        'https://github.com/TheAlgorithms/C-Plus-Plus.git',
        'https://github.com/TheAlgorithms/Ruby.git',
        'https://github.com/TheAlgorithms/C-Sharp.git',
        'https://github.com/TheAlgorithms/PHP.git',
        'https://github.com/TheAlgorithms/Swift.git',
        'https://github.com/TheAlgorithms/Kotlin.git',
        'https://github.com/TheAlgorithms/Rust.git',
    ]
    if args.repos:
        print(args.repos)
        repos = args.repos[0].split(",")
    if args.protobuf_path and len(repos) > 1:
        print("Can't copy protobuf output for more than one report.")
        exit(1)
    print("Repos to process count: {}".format(len(repos)))
    print("Repos to process: {}".format(repos))
    print("Skip load to database: {}".format(args.no_db))
    for repo in repos:
        protobuf_folder = gen_analyze_report(git_url=repo)
        print(protobuf_folder)
        if args.protobuf_path:
            copy_tree(protobuf_folder, args.protobuf_path)
        if not args.no_db:
            pr = ProtobufReader(protobuf_folder)
            pr.load_to_database()
