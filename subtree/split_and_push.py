from git import Repo
from termcolor import colored
import datetime
from tqdm import tqdm

# default values
target_branch = "prismui-master"
remote = "prismui-master-complete3"

fail_msg = colored("Failure: ", "red")
succ_msg = colored("Success: ", "green")

def date_from_git(authored_date):
    return datetime.datetime.fromtimestamp(authored_date).date()

def first_day_of_next_month(date):
    date = date.replace(day=1)
    if date.month == 12:
        return date.replace(year=date.year+1, month=1)
    else:
        return date.replace(month=date.month+1)

def first_days_between_start_and_end(start_date, end_date):
    first_day_list = []
    current = first_day_of_next_month(start_date)
    while current <= first_day_of_next_month(end_date):
        first_day_list.append(current)
        if current.month == 12:
            current = current.replace(year=current.year+1, month=1)
        else:
            current = current.replace(month=current.month+1)
    return first_day_list

def main():
    # 0. Displaying default values for target_branch and remote
    print("0. Current default values for target_branch and remote are as follows: ")
    print(colored("target branch: ", "yellow"), target_branch)
    print(colored("remote name: ", "yellow"), remote)
    print("Make sure the above default values are correct, you can modify them in this script.")
    input("If they're correct, press any key to continue.")
    print()

    # 1. Detecting git repo
    repo_path = input("Please enter the absolute path to the local repo:\n")
    try:
        print("1. Detecting git repo...")
        r = Repo(repo_path)
        print(succ_msg + " git repo found.")
    except Exception as e:
        print(fail_msg + "Unable to initialize git.Repo object, please double check the path.")
        exit()
    print()

    # 2. Detecting target branch
    print("2. Detecting target branch %s..." % target_branch)
    total_num_commits = r.git.rev_list('--count', target_branch)
    print(succ_msg + " total number of commits on target branch: %s" % total_num_commits)
    print()

    # 3. Finding the first commit and the last commit 
    print("3. Finding the first and the last commit...")
    print(succ_msg)
    first_commit = r.commit(r.git.rev_list("--max-parents=0", target_branch))
    start_date = date_from_git(first_commit.authored_date)
    print("First commit date: " + str(start_date))
    last_commit = r.commit(r.git.rev_list("-1", target_branch))
    end_date = date_from_git(last_commit.authored_date)
    print("Last commit date: " + str(end_date))
    print()

    # 4. Computing the number of pushes needed (one per month)
    print("4. Computing the number of pushes needed (one per month)...")
    first_days = first_days_between_start_and_end(start_date, end_date)
    print(succ_msg + str(len(first_days)))
    print()

    # 5. Pushing to the remote
    print("5. Pushing to the remote...")
    last_hexsha = None
    for first_day in tqdm(first_days):
        hexsha = r.git.rev_list("-1", "--first-parent", "--before=%s" % str(first_day), target_branch)
        if hexsha == '':
            continue
        if last_hexsha != None:
            num_new_commits = r.git.rev_list("%s..%s" % (last_hexsha, hexsha), "--count")
        else:
            num_new_commits = r.git.rev_list(hexsha, "--count")
        print(first_day, hexsha, 'pushing %s new commits' % num_new_commits)
        last_hexsha = hexsha
        r.git.push(remote, '+%s:refs/heads/master' % hexsha)
    print(succ_msg)


if __name__ == "__main__":
    main()
